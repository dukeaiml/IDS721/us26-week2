# Us26 Week2

# AWS Lambda Function in Rust

This AWS Lambda function written in Rust processes and transforms sample data.

## Usage

1. Clone the repository and navigate to the project directory:

    git clone https://gitlab.com/dukeaiml/IDS721/us26-week2.git \
    cd my_lambda_project

### Build the Lambda function using Cargo Lambda:

    cargo lambda build --release

### Package the Lambda function for deployment:

    cargo lambda package


Deploy the Lambda function using the AWS Management Console or AWS CLI.

Invoke the Lambda function with sample data to see it in action.

### Sample Data

The function receives JSON events and currently echoes the input event as output. You can modify the process_event function in src/main.rs  to perform your desired data processing or transformation.

### Requirements
1 - Rust and Cargo installed\
2 - AWS account with necessary permissions to deploy Lambda functions

### Deployed lambda function on AWS
![Image Alt Text](screenshot/3.png)

### Input payload
![Image Alt Text](screenshot/2.png)

### Response payload
![Image Alt Text](screenshot/1.png)
